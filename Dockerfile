FROM frekele/ant:1.10.3-jdk8 AS builder
LABEL maintainer="david-derigent@sword-group.com"

RUN mkdir -p /app/build
WORKDIR /app/build

COPY . .
RUN ant -f ./build.xml dist

FROM tomcat:8.5.54-jdk8-openjdk

# Timezone
ENV TZ=CET

WORKDIR /usr/local/tomcat
COPY --from=builder /app/build/dist  ./webapps
COPY --from=builder /app/build/lib/sesame  ./webapps
COPY --from=builder /app/build/lib/javax.mail-1.5.1.jar  ./lib
COPY --from=builder /app/build/conf/thesaurus.xml  ./conf/Catalina/localhost/
COPY --from=builder /app/build/conf/tomcat-users.xml  ./conf/


